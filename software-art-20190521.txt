SOFTWARE ART

le software art, ou art logiciel

avec le concept de "software art" ou d'art programmé, je propose ici de valoriser un ensemble de productions prenant la forme d'un programme en tant que forme d'art relevant d'un medium spécifique: la programmation

- programme/logiciel

Un programme est une chose présente "à l'intérieur" d'un ordinateur et permettant d'accomplir des tâches qui sans lui n'étaient pas effectuables par l'ordinateur. Dans l'imaginaire collectif, le programme est nécessairement utilitaire et son but est parfaitement intelligible. À la question qu'est-ce qu'un progamme, un quidam répondra facilement par l'exemple: "Word, ou Firefox par exemple", enfermant à jamais la notion de programme dans un statut d'outil pratique, au mieux.

Une meilleure définition du programme est de mon point de vue: un ensemble de processus et d'algorithmes mis en forme de manière à les rendre opérable par une machine binaire (on arrive ici très proche de la théorie des nombres calculables de Turing). Il est très important de noter que dans cette définition, rien n'est dit sur la finalité ou l'intentionalité du programme. Ces paramètres sont exogènes au programme lui-même, bien qu'ils seront inscrits clairement dans son code source!
La machine binaire ne comprends en effet pas la finalité, elle ne peut que traiter des données en suivant un ensemble d'instructions décrite par la programme. Pour faire un parallèle, le livre en tant qu'objet tangible constitué de fibres de bois et d'encre ne contient pas en son sein autre chose que sa propre matérialité. L'idéologie ou les concepts qu'il contient sont extérieur à sa matérialité. Les humains se servent de cette matérialité pour transmettre des idées, concpets et idéologies.
La nature du programme est autre que celle du livre, basée sur du silicium et de l'électricité. En tant qu'objet technique, il est bien entendu emprunt de la technique et donc de la pensée de ceux qui l'ont concu. Mais la suite de 0 et de 1 le constituant ne dit rien à priori sur les intentions de son concepteur.
Cette absence de but dans la définition du programme est importante. Elle permet de lui donner un corps propre, une physicalité en somme. Le programme n'est pas lié nécessairement à la notion de tâche, de performance ou de production. Il est une ensemble d'instruction compréhensible par le processeur d'un ordinateur, ni plus ni moins.

Un autre concept important à déconstruire quand on parle de programmes est la notion d'utilité. "Un programme, ca sert à faire qqle chose". En lui assigant cette finalité ultra-utilitariste, le sens commun enlève à cet objet tout potentiel artistique, puisqu'il semble communément admis que l'art se reconnait au fait qu'il ne "sert" à rien, n'a pas d'utilité directe comme celle d'allumer un feu ou de soulever une roche.
Hors le programme, en tout cas ceux présenté au novices comme en étant, est réduit à l'accomplissement de tâches administratives (les suites bureautiques), à la navigation sur le net ou à la lecture de média (musique, vidéo et images). Sa versatilité intrinsèque le fait le plus souvent disparaitre, lui donnant tout au plus le statut de vecteur, de cadre permettant l'accès à autre chose, au média par exemple. Au mieux il fonctionne, au plus il disparait. 
Prenons VLC par exemple. Son succès est dû au fait qu'il est capable de décompresser la majorité des codecs existants et de le faire d'une manière assez fiable pour que les utilisateurs en oublie sa présence. "Je regarde une vidéo" et non "Je regarde le résultat de la décompression d'un fichier vidéo par VLC".

C'est ici pour la première fois que le jeu vidéo et la courte période des DVD et CD interactifs sont importants pour le software art. Ils perturbent cette vision utilitariste des machines et, de ce simple fait, perdent très rapidement le statut de programme!
Parce qu'ils ne présentent plus comme répondant à une utilité partico-pratique, le grand public ne les percoit plus pour ce qu'ils sont: un jeu vidéo est un jeu vidéo, ce n'est pas un programme.
Pourtant, le jeu vidéo comme le traitement de texte répondent parfaitement à la définition fondamentale de ce qu'est un programme. Un jeu vidéo est un programme d'un genre un peu spécial mais qui n'en reste pas moins une mise en forme de processus et d'algorithmes! 

L'utilité d'un programme n'étant pas inscriptible dans code source: le gameplay d'un jeu, ou son déroulement narratif relève quasiment toujours de fichiers et non du programme en lui-même. En temps qu'objet industriel (objet abstrait selon simondon), le jeu vidéo est une optimisation extrême du potentiel d'un ordi.

- origines: demo scene
[historique revisité]

- art algorithmiques
[historique revisité]

- la programmation et la forme du programme

les programmes sont invisibles pour une majorité d'humains: tout comme le marteau ou la vis, ils n'ont pas été concu et pensé par des humains mais sont une forme naturelle au sein d'un ordinateur
l'interface graphique, développée par apple au début des années 90 a transformer l'écran de télévision en un outil privé 

- art(s) numérique(s) // de la séparation software/hardware

c'est au travers des arts numériques que j'ai conscientisé le fait que la programmation est un medium dont la forme la plus démocratique est la diffusion sous forme d'exécutable
les arts numériques (ici je parle exclusivement des formes installatives, performatives et galeries) se bornent en effet à produire des objets basé sur du software mais concues en relation forte avec le hardware, hardware spécialisé (casques vr, raspberry, capteurs exotiques, machines-robot, etc.), donnant aux pièces une forme figée
pour la forme recherchée par les artistes utilisant ce mélange hard/soft, ca fait sens
par contre, une tension énorme se fait quand il s'agit d'art "purement" algorithmique (conmme genealogy) -> l'ordinateur et l'écran sont non-essentiels à la pièce, ne servent qu'à attester de son existence et du processus en cours
même si la représentation graphique est importante et constitue une facette essentielle de la pièce, son support importe moyennement: l'utilisation de projecteur de faible qualité n'empêche pas la pièce d'exister, pas plus que les limitations matérielles de la machines
le programme peut s'adapter à son environnement/hardware: modification de la résolution, du nombre d'éléments affichés, etc.

genealogy n'a pas été pensé en tant que software art: il n'est donc pas facilement diffusable, ne prévoit pas d'interfacage de configuration ou d'automatismes permettant l'ajustement automatique des paramètres

- la version, et le mécanisme d'évolution particulier des programmes

à l'opposé de la majorité des arts plastiques traditionnels qui ont tendances à figer des images sur un support, l'art programmé est essentiellement évolutif. Cette évolution est induite par l'évolution du matériel et des OS premièrement, mais il est aussi une manière de raffiner et ramifier une proposition initiale au fil du temps, de développer de nouvelles propositions au sein du cadre initial, de revoir complètement des pans entiers de la pièce initiale
de ce fait, et bien qu'il en soit de prime abord très lointains, l'art programmé s'accroche aux arts vivants et à la musique
- arts vivants parce qu'une re-présentation n'est jamais identique à la précédente (changement de lieu, d'énergie chez les acteurs et dans la salle) - il y a des impondérables avec lesquels joué pour que faire exister la pièce - le parallèle avec l'art programmé donne: changement de machine, d'écran, de souris, smartphone dans le bus, laptop dans le lit ou desktop au bureau sont autant d'impondérables sur lesquels l'artsite-programmeur n'à aucune autorité
- musique parce qu'il y a séparation de l'acte d'écriture et des l'acte de performance - acte de performance: rejouer la partition, que ce soit en privé ou en publique, par opposition à l'enregistrement et à l'écoute sur support. Puisqu'il y a non pas une forme figée sur un support mais un acte humain (instrumenté) nécessaire à l'apparition de la pièce, les conditions de cet acte l'influencent, influencent sa forme, ce qu'elle évoque ou raconte. En ce sens, la musique écrite est une proposition plus qu'une forme figée. 

[note] les concours tel le reine elisabeth ont tendance à contrôler un maximum les conditions de la performance pour permettre uen certaine qualite de la pièce, qualité définie idéologiquement et donc politiquement comme étant les "meilleures et seules bonnes conditions à l'apparition de l'art", s'assurant du même coup que ces conditions demandent tellement de moyens et d'infrastructures qu'il est impossible de les reproduire ailleurs que dans des mileiux privilégiés, liés à l'élite culturelle inetrnationale - l'aspect politique devient donc évident: de tels moyens deviennent symboliques de la puissance et de l'influence d'un pays ou d'une zone géographique (l'europe par ex.)
d'autres types de musiques écrites ne nécessitent pas cet écrin et d'ailleurs n'ont pas été concues pour y être performée: le vj, le punk, les musiques tribales, etc, etc

ce qu'implique la version: modifier une pièce au cours du temps, lui ajouter ou retirer des fonctionnalités, contient le risque important de la détruire, de la déformer jusqu'à ce qu'elle perde son essence à force de torsion - ce danger la garde vivante, non-déjà faite, in-finie. Elle n'apparait que si elle est exécutée, ce qui la rend déjà dépendante au moins du hardware qui l'accueille, mais en plus elle est modifiée par son/ses concepteurs au fil des mois/années

-- exogène / endogène
Ces modifications sont parfois nécessaires à son "bon fonctionnement": changements exogènes - ils peuvent influencer l'essence de la pièce sans que le/les concepteurs le conscientise nécessairement, tout concentrés qu'ils sont sur la mécanique infiormatique
D'autres sont guidé par le regards du concepteur sur sa pièce: ceci n'est pas/plus "juste" (je ne le vois plus comme ca), il manque ceci pour la pièce soit plus proche de mon intention, je viens de comprendre qqle chose qui rends plus fluide la circulation des concepts au seins du programme: changements endogènes, guidés par l'intention des concepteurs et la sensation d'aboutissement dans la pièce

- de l'idée matérialisée au sens propre du terme

[voir écriture parole et code!]

- réappropriation et politisation de l'art programmé

renaissance: la peinture et la musqiue sont au service de l'idéologie domaninate, des royauté et du pouvoir religieux

post-industrielle: l ínformatique est au service de l'idéologie dominante, puisqu'elle y voit un moyen très efficace de conforter et de développer son influence
par contre, `l'opposé de la peinture et de la musique qui sont intuitivement percu comme propices à l'expérience esthétique, l'informatique étant issue et intimement liés à l'ingénieurie et à la recherche scientifique, son aspect esthétique est absente de l'imaginaire collectif et donc des instances politiques (à l'exeption de la cellule des arts numériques en belgique!)
comme si la peinture ne pouvait être qu'utilitaire, pour le batiment ou l'automobile, d'abord utilitaire et puis légèrement artistique
[gainsbourg]: les arts majeurs demandent une formation par des maitres - en ce sens, l'art programmé ne s'improvise pas car il demande une conformation non-natuelle, hardue et peu promue par l'enseignement, de l'esprit et donc de l'intention
tout comme les autres mediums, il y a une attirance subjexctive des concepteurs pour le medium, parce qu'il contient en son sein des mécanismes qui permettent à l'intention d'apparaitre dans sa forme complète

récupération par l'industrie des programmeurs/artistes: alors que les instances cuturelles traditionnelles telles les musées et les politques ne comprennent pas bien les implications de ce médium, les industriels ont eu parfaitement compris le profit qu'ils pouvaient tirés de la sensibilité avec laquelle les progarts manipulent et créent avec ce médium - pris dans le tourbillon de la course à l'armement technique, ils sont exploités, parfois consciemment (hein!), et instrumentalisés > ils deviennent des représentants du produit, on met en avant leur utilisation des dernières fonctionnalités de tel ou tel outil
ca se retrouve dans la majorité des softs et frameworks commerciaux: la pièce du moment

- de l'humilité du programmeur

au cours des années de présence dans le secteur et de l'explorations des structures, il m'apparait un fait indéniable: les programmeurs sont humbles - le médium qu'ils choisissent est un des plus complexes à manipuler mentalement, il demande une ascèse importante, un retrait du monde et de l'instanéité pour ménager le temps nécessaire à la mise en forme d'algorithmes et de process
la part artistique doit être adaptée à cette temporalité, devenir un choix profond, attentif et patient, plus l'explosion brève et intense demandée à l'acteur ou dans le dessin par ex.
cette complexité du médium fait très vite prendre conscience au programmeur de ses propres limitations intellectuelles et questionne en permanence sont engagement et ses valeurs
en même temps, cette habitude de la complexité rend sensible son esprit aux nouvelles approches et à de nouveaux enjeux. étant sans cesse au prise avec des formes d'organisation de processus, les programmeurs sont rodés à la résolution de problèmes - lui poser la question de comment faire est le rejoindre au coeur de ses procupations journalières.
Un autre aspect extrêmement positif du monde de la programmation est l'idée de base du "tout est possible". L'expérience du médium développe une compréhension profonde des possibilités du medium, et donc une conscience accrue du peu de limitation effective de ce qui effectuable au moyen d'une machine binaire.
Au contraire des mathématiques dures, l'informatique est un espace partiellement fondé sur la débrouillardise et la bidouille. Une solution informatique est bien entendu validée par la "perfection" de son code sans pourtant que ce soit obligatoire! Certaines approches sont en effet basée sur un contournement astucieux des limitations de la technologie. Un bon exemple est la génération de nuages dans les moteur 3d. La simulation rigoureuse étant extrêmement coûteuse en processeur quand elle s'appuie sur des modèles chaotiques par exemple, une frange de programmeurs se détachant du monde scientifique ont pu mettre au point des approximations basées sur des éléments n'ayant à priori aucun rapport avec la structure des nuages, approximations d'assez bonne qualité visuelle pour rendre la "suspension de crédulité" confortable.

Humilité, curiosité, pensée hétérodoxe et ouverture d'esprit donc.

Ce ne sont évidemment pas des traits obligatoirement présents chez tous les programmeurs, mais c'est plutôt une ambiance de milieu qui favorise cet état d'esprit

- art programmé et jeu vidéo

L'art programmé pré-existe ou du moins coincide avec l'apparition du jeu vidéo. Mais pourquoi relier ces 2 champs, qu'ont-ils en communs?
Dans son histoire, le jeu vidéo c'est de plus en plus distancié de machines spécifiques. Les consoles de jeu modernes en sont un bon exemple: que ce soit pour la ps4 et la xbox one, le hardware diffère très peu de celui utilisé dans les ordinateurs.
Depuis l'arcade où il fallait se déplacer dans un lieu spécifique pour pouvoir jouer, le jeu est entré dans les foyers grâce à l'avènement des consoles et des micro-ordinateurs. Leur forme a aussi été influencée par cette évolution. De jeux basés sur le stress et essayant de faire perdre le plus vite possible le joueur pour qu'il remette de l'argent dans la machine, on est passé à l'achat du jeu et du matériel. À partir de ce moment, les jeux proposent une expérience plus longue, moins tendue, aboutissant aujourd'hui à des formes sans tensions, comme mountain ou beyind two souls.
Favoriser par une vague de DIY émanant du néo-capitalisme qui prône le modèle de l'entrepeneur comme forme utlime de l'évolution de l'homme économique (le "self-made man" doublé d'un inventeur génial, le "maker"), un nouveau groupe de concepteurs s'est formé en marge des mastodontes de l'industrie vidéoludique. Ce groupe appelé "indépendant", en général de très petites équipes, avait besoin d'outils pour ses créations.
Héritiés de la même mouvance, de jeunes et moins jeunes sociétés de développement d'outil dédiés au jeu vidéo (game engines) ont revu leur modèle commercial, jusqu'ici basé principalement sur l'achat de licenses. Les indépendants n'ayant en général pas de financiers pour les soutenir, ils ne pouvaient pas investir dans l'achat de licenses au prix pharaoniques. En revoyant leur modèle commercial pour le basant sur des royalties liés à la vente de jeux (percu APRÈS l'utilisation de l'outil) plutôt que sur des licenses percues PENDANT l'utilisation de l'outil, ces qqles sociétés ont injecter de la nitroglycérine dans le secteur indépendant.
Ce phénomène a créé une bulle spéculative comparable à celle du net des années 2000, provoquant une ruée vers l'or d'un grand nombre de créateur non-adaptés aux exigences esthétiques et techniques des majors. 
Même si cet engouement a très rapidement saturé de jeux tous les canaux de distribution et dans le même mouvement épuisé une grande partie des concepteurs indépendants, il a l'avantage d'avoir permis la modification du rapport aux outils de création. D'une caste capable de "se payer" les meilleurs programmes, le jeu vidéo a démocratisé l'idée qu'un accès gratuit aux programmes est nécessaire et bénéfique à la création.

Cette bulle a aussi été rendue possible par les nouveaux canaux de distribution de jeux en ligne tels steam. Ces canaux permettent de se passer de support physique et du liens avec la grande distribution de biens tangibles.
En marge de ces deux groupes dominant (l'industrie et les indépendants), un autre type de profil de concepteurs s'est vu tout d'un coup doté d'outils industriels. Ce profil est très difficilement résumable en qqles mots, puisqu'il s'agit ici d'individus hétérogènes, sans idéologie commune et sans nécessairement de volonté commerciale affirmée, regroupant des hobbyistes, des amateurs, des artistes de tout bords, des membres de l'idustrie et de l'indépendant.
Ils utilisent le medium pour expérimenter de nouvelles approches, qu'elles soient techniques, esthétiques ou narratives.
Ces objets bizaroïdes ne se diffusent évidemment pas par les plateformes commerciales standards, voyant (à tort comme à raison) leur arrivée comme un risque pour leur crédibilité, ces objets étant loin d'être parfaitement fonctionnels. Je pense aussi qu'ils sont dangereux pour l'image du medium en lui-même. Si steam valide et diffuse un objet que très peu de ses clients reconnaitront comme un jeu vidéo, le risque symbolique et donc financier augmente. Comme toute institution, les plateformes de diffusion que sont steam et l'istore vont au travers de leur choix conforter leur vision du medum, vision mélangeant les préoccupations financières à des considérations artistiques.
Il est a noté que ces institutions ont au moins le mérite d'être honnêtes sur leur motivations, permettant aux créateurs de se positionner plus rapidement que dans d'autres institutions culturelles, engluées dans des logiques de cours. "Ici on fait de l'argent" semble noté en lettre d'or sur leur fronton, avec une absence de scrupules propres au néo-capitalisme. Cette clareté est contrebalancée par une relative flexibilité quand à leur vision du médium. Si un nombre de clients suffisant valide un objet, elle sera diffusée sur la plateforme, sans qu'une caste d'élite officielle puisse bloquer la diffusion. Il existe une caste essayant de définir ce qu'est le jeu vidéo, regroupant les directeurs des majors et les journalistes de la presse spécialisée, mais elle n'a pas d'accès direct aux plateformes.
Ces plateformes ont donc participer à la diffusion des indépendants, en général assez respectueux des dogmes du secteur, en minimisant à quasiment 0 les coûts de diffusion. Loin d'être altruistes, ces plateformes ont utilisé le même modèle que celui des outils: la perception de royalties sur les ventes, plutôt que l'achat d'un droit d'accès.
Aussi peu coûteuses que puissent être ces plateformes, les concepteurs hétérodoxes ayant typiquement des audiences très réduites, n'ont par contre que très peu de chance d'arriver à diffuser par ces canaux. La simple présence d'un filtrage les discréditent.
En parallèle aux modèles de steam et de [EA?], d'autres plateformes sont apparues dans leur ombre, telle itch.io et indieDB. Ici, pas de droit d'entrée autre que la création d'un compte. Même si elles utilisent un modèle commercial inspiré des majors, le non-filtrage des contenus diffusés (il y a modération, et non filtrage) a rendu possible la constitutions de niches expérimentales à côté de production mainstream.
L'intérêt de ce genre de pratique est la co-influence des milieux. Plutôt que de se retirer dans des caves obscures, les hétérodoxes co-existent et intéragissent avec les hortodoxes, favorisant une émulation et la réappropriation des problématiques dans les deux sens!

C'est dans les recoins de ces plateformes que se retrouvent des objets étranges, manipulant et explorant le médium. Voir par exemple les ProcJam, type de gamejam dédiées à des recherches procédurales et non-nécessairement vidéoludiques.

Il existe d'autres platefroems de diffusion d'applications dont certaines anciennes (sourceforge par exemple, plus récemment github). Ces plateformes sont néanmoins dédiées aux programmeurs et rendent difficiles l'accès au grand public.
L'intérêt majeur de itch.io par exemple est la présentation simplifiée et harmonisée de toutes les productions. En obligeant un format de fiche unique, des programmes expérimentaux autrefois difficilement accessible même en connaissant l'adresse du site, deviennent faciles à installer et à expérimenter.

- de l'art pour ceux qui n'en veulent pas

[mouais] En se mélangeant de la sorte aux productions mainstream, les hétérodoxes ont la chance de pouvoir proposer leurs objets à un public plus varié que leurs 3 amis. La plateforme se présente comme un diffuseur de jeux, au contraire d'un musée ou d'une galerie qui eux se présentent comme détenteur d'une vérité. Les objets qui y sont présentés ne sont donc pas chargés A PRIORI d'une valeur symbolique importante, comme le sont les artefacts montrés dasn un musée.
Les objets sont là, se proposent à l'essai souvent gratuit et n'impose pas nécessairement la manière de les appréhender. L'expérimentation de ses objets n'est donc pas conditionnée d'emblée, laissant les utilisateurs plus libre (on est quand même sur une plateforme de jeu vidéo, amenant avec elle tous les archétypes qui lui sont liés) de la manière dont ils valident ou pas tel ou tel objet.
La pression sur les concepteurs est assez réduite (comme leur revenus): n'ayant pas à passer les filtres, leur objets n'ont pas nécessairement à se conformer aux attentes d'un milieu, chose rare pour des objets ayant une prétention artistique (ou pas). Le choix d'appliquer tel ou tel critère devient personnel et montre les croyances et envies du concepteurs plus que sa capacité à les apppiquer.

- itch.io comme laboratoire de recherche sur le medium

- art populaire (non, pas le pop art!)

Le jeu vidéo n'est pas validé par les institutions culturelles standards, ce qui ne le libère pas du tout de l'emprise d'agents normalisateurs.
Rarement financé par des fonds publics (l'exception Tale of tales par exemple), les concepteurs de jeu vidéo ont une forte tendance à considérer leur oeuvres comme des objets de consommations, subissant les lois de l'offre et de la demande, déformant dans ce mouvement leur intentions artistiques au profit de normes commerciales. Derrière ces préoccupations pécuniaires, nécessaires à la poursuite d'une activité même si il me semble dangereux d'appliquer les recettes du néo-capitalisme à des objets culturels, se cache un problème bien plus fondamental qui cause au jeu vidéo un énorme dommage symbolique. Pour réduire le risque économique, ce n'est pas seulement l'organisation du travail qui est emprunte de capitalisme, mais l'essence même de l'objet: reproduction des schémas de domination, qu'ils soient de genre ou racial, réduction de l'interaction au principe d'affrontement (ce qui rejoint la domination) et donc manichéisme simpliste, conformation des aspects visuels aux possibilités des outils, et foule d'autres joyeusetés.
En se voulant populaire, accessible à tous et non-soumis à l'approbation des instances culturelles, le jeu vidéo a développé d'autres travers propre aux sous-cultures. Sa particularité vis-à-vis de la bande dessinée ou du cinéma d'animation est qu'il génère énormément d'argent, et se retrouve par là beaucoup plus sous l'emprise du marché que les autres.
D'un autre côté, la taille et les chiffres du marché du jeu vidéo favorisent l'éclosion de nouvelles formes à sa périphérie. Les miettes laissées par les majors suffisent parfois à nourrir qqles créateurs isolés, capables par la modestie de leur mode de vie, de travailler de longues années sans promotion et sans soutient étatique. Intensément basé sur la copie, le jeu vidéo est avide d'innovations, qui permettront d'ajouter des arguments aux discours des commerciaux. Ces innovations sont de différents genre:
- meilleures performances du programme, la majorité du temps lié aux efforts des sociétés construisant les game engine, qui se traduit par plus "beau", entendu par là réaliste et détaillé, plus d'éléments animés et réactifs, etc.
- nouveaux périphériques, tels kinect, casques de reálité virtuelle, etc., faisant évoluer l'interface homme-machine
- intégration de techniques "cutting-edge": simulation de fluides, effets atmosphériques et autres
Ces innovations peuvent aussi concerner le gameplay, c'est-à-dire le type d'interaction qui est proposée au joueur: plus de contôle, plus d'interaction avec les autres joueurs quand il s'agit de jeux en ligne.

En soldat obéissant de l'idéologie dominante, le jeu vidéo promeut sans vergogne le progrès par l'ingénieurie et les stratégie de commercialisation (pub, tournois). Il ne se présente pas comme produisant des objets d'art mais comme une sous-culture "underground", dégagée de la pédance des beaux-arts et autres arts officielement reconnus, alors qu'il s'agit aujourd'hui d'une industrie plus grande que le cinéma! Le risque de ce positionnement est que son influence symbolique sur les sociétés passe inapercue, et que les modèles qu'il promeut volontairement avec une harrogance et une assurance digne d'un participant de Big Brother s'inscrivent profondément dans des imaginaires assèchés par la saturation d'informations. En se voyant comme "fun", "pas chiant", donnant à son public se qu'il attend, c'est-à-dire du défoulement et du pulsionnel, le jeu vidéo industriel déchire le tissu social tout autant que la télévision de la fin des années 90 l'avait fait [voir "temps de cerveau disponible"].

Il existe bien évidemment des zones libres dans ce grand marché vidéoludique. L'underground d'un milieu qui se présente comme l'étant.
Isabelle Arvers, Babycastle et pouet.org.

- diffusion!!

